import os  # for datetime
import hashlib  # for id
import json  # Pretty printing resp
import sys # build.date.utc

LINEAGE_OTA = "lineage_laurel_sprout-ota-eng.arru.zip"

if __name__ == '__main__':
    resp_dict = {}
    resp_dict['response'] = []

    ota_dict = {}
    ota_dict['datetime'] = int(sys.argv[1])
    ota_dict['filename'] = LINEAGE_OTA
    ota_dict['id'] = hashlib.md5(open(LINEAGE_OTA, 'rb').read()).hexdigest()
    ota_dict['romtype'] = "UNOFFICIAL"
    ota_dict['size'] = os.path.getsize(LINEAGE_OTA)
    ota_dict['url'] = "https://gitlab.com/MasterAwesome/laurel_sprout-builds/-/raw/master/lineage_laurel_sprout-ota-eng.arru.zip"
    ota_dict['version'] = "18.1"

    resp_dict['response'].append(ota_dict)

    with open('response.json', 'w') as fp:
        fp.write(json.dumps(resp_dict, indent=4))
